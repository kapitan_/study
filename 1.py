

#импортирую функцию из другого файла в той же папке(test)#
import lib 
a='hello'
lib.test(a)
import numpy as np
a=np.array([255], dtype=np.int8)
b=a.astype(np.int32) #не работает
print(b+1)
print('как задавать массивы в numpy')
a=np.linspace(0, 1, 5)
b=np.linspace(2, 3, 5)
print(a)
print(a+b)
print('расстояние между двумя векторами')
cc=np.sqrt(np.sum((a-b)**2))
print(cc)
'''
массив с элементами типа bool может использоваться как
маска для другого массива, чтобы изменять только часть элементов например
'''
print('все элементы больше 0.5 умножить на 2')
bl=a>0.5
a[bl]*=2
print(a)
print('из каждого элемента вычесть предыдущий')
a=np.linspace(0, 1, 5)
print(a[1:]-a[:-1])
print('перевернуть массив')
print(a[::-1])
'''если массив b является частью массива а, то после изменения b
изменяется и а. иногда это полезно, но этого можно избежать 
'''
print('иерархические отношения')
print(a)
a3=a[:2]
a3+=1
print(a)
print('эта проблема решается функцией np.copy')
a=np.linspace(0, 1, 5)
a3=np.copy(a[:2])
a3+=1
print(a)
'''
ДЗ: загрузить из библиотеки sklearn датасет ирисы фишера, сделать массив с z score для каждого значения
'''
from sklearn.datasets import load_iris
data = load_iris()

x=data["data"]
print(x)
