#ДЗ: загрузить из библиотеки sklearn датасет ирисы фишера, сделать массив с z score для каждого значения

import numpy as np
from sklearn.datasets import load_iris
data = load_iris()

x=data["data"]
print(x[0][3])
a=[x[i][0] for i in range(len(x))]
b=[x[i][1] for i in range(len(x))]
c=[x[i][2] for i in range(len(x))]
d=[x[i][3] for i in range(len(x))]
ava, avb, avd, avc = 0, 0, 0, 0
for i in range(len(x)):
    ava+=a[i]
    avb+=b[i]
    avc+=c[i]
    avd+=d[i]
ava, avb, avd, avc = ava/len(x), avb/len(x), avd/len(x), avc/len(x)
#print(ava, avb, avd, avc)
sa, sb, sc, sd = 0, 0, 0, 0
for i in range(len(x)):
    sa+=((a[i]-ava)**2)
    sb+=((a[i]-avb)**2)
    sc+=((a[i]-avc)**2)
    sd+=((a[i]-avd)**2)
sa, sb, sc, sd = np.sqrt(sa/len(x)), np.sqrt(sb/len(x)), np.sqrt(sc/len(x)), np.sqrt(sd/len(x))
for i in range(len(x)):
    a[i]=(a[i]-ava)/sa
    b[i]=(b[i]-avb)/sb
    c[i]=(c[i]-avc)/sc
    d[i]=(d[i]-avd)/sd
#print(a)
from scipy import stats
print('результат функции из библиотеки')
print(stats.zscore(x, axis=0))
# параметр axis при нуле считает по столбцам, при единице по строчкам
print('мой результат')
for i in range(len(x)):
    x[i]=[a[i], b[i], c[i], d[i]]
print(x)
