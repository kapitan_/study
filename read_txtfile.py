import numpy as np
import matplotlib.pyplot as plt

file = open("D:/Сашааааа/Programming/data/rat 3 11.10.2013.txt", mode="r", encoding="windows-1251")
filecontent = file.read()
file.close()

data = {}

channel_names = []

lines = filecontent.split("\n")
for idx, line in enumerate(lines):

    if (line.find("Sample Period") != -1):
        sample_period = line.split(" ")[2]
        data["Sampling_period"] = float(sample_period)

    if len(line)>0 and line.find(":") != -1 and line[0] == " ":
        channel_name = line.split(" ")[-2]
        # print(channel_name)
        data[channel_name] = []
        channel_names.append( channel_name )

    if len(line)>0 and line.find("    ") == 0 and line.find("#") == -1:
        sl_signs = line.split("      ")
        for idx_sig, sigs in enumerate(sl_signs[1:]):
             data[channel_names[idx_sig]].append(float(sigs))


    # if idx == 30:
    #     break


# print(data)
for key in data.keys():
    data[key] = np.asarray(data[key])

print(data["ms"].size)

plt.plot(data["ms"])
plt.show()
